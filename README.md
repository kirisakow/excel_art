**WARNING** This library is still under development and intended for experimental purposes only.

# Excel Art: convert image to a colored Excel spreadsheet

This is a Python 3.10+ script that replicates a given image pixel by pixel as a spreadsheet, by colouring each cell as the corresponding pixel.

## Usage

To print a quick user guide, run the script with no arguments: `python excel_art.py` or `python /path/to/excel_art.py`

### In UNIX console:
```py
python excel_art.py --path_to_img "python_logo_128x128.jpg"
```
### In PowerShell console:
```py
python excel_art.py --path_to_img "python_logo_128x128.jpg"
```
### In DOS console:
```py
python excel_art.py --path_to_img "python_logo_128x128.jpg"
```

## Options

|||
|---|---|
| `--path_to_img` | required |
| `--desired_cell_height_in_pixels` | optional; default: `5.5` |
| `--desired_art_width_in_columns` | optional; default: `100`; max: `1024` |
| `--path_to_output_file` | optional; default: `./excel_art.xlsx` |

## Example

Running the script with the following parameters:

```sh
python excel_art.py --path_to_img python_logo_128x128.jpg --desired_art_width_in_columns 128 --desired_cell_height_in_pixels 6
```

...produced the following output and file:

```sh
Image columns: 100%|███████████████████████| 128/128 [00:01<00:00, 104.42it/s]
Saving as 'excel_art_python_logo_128x128.jpg_rendered_as_128x128.xlsx'... OK
```

<img src="screenshot.png" alt="drawing" width="80%"/>
