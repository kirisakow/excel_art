# -*- coding: utf-8 -*-

import argparse
import os, sys
import openpyxl
from openpyxl.styles import PatternFill
from openpyxl.utils import get_column_letter
from pathlib import Path
from PIL import Image
from tqdm import tqdm, trange


def get_file_name_from_path(path: str) -> str:
    """
    Universal extractor of file name from any kind of path
    Source: https://stackoverflow.com/a/8384788
    """
    head, tail = os.path.split(path)
    return tail or ntpath.basename(head)


def print_user_guide() -> str:
    user_guide = f"""

This is a Python 3.10+ script which replicates a given image as a spreadsheet, by colouring each cell as the corresponding pixel.

Example:
"""
    line_cut_sign = {'UNIX': '\\', 'PowerShell': '`', 'DOS': '^'}
    for shell in line_cut_sign.keys():
        user_guide += f"""
- In {shell} console:

python {get_file_name_from_path(sys.argv[0])} {line_cut_sign[shell]}
  --path_to_img "python_logo_128x128.jpg" {line_cut_sign[shell]}
  --desired_cell_height_in_pixels 5.5 {line_cut_sign[shell]}
  --desired_art_width_in_columns 100 {line_cut_sign[shell]}
  --path_to_output_file ./excel_art.xlsx
"""
    return user_guide


def dec_to_hex(n: int) -> str:
    return f'{n:02x}'


def rgb_dec_to_rgb_hex(rgb_dec: list | tuple) -> str:
    return ''.join(
        [dec_to_hex(n) for n in rgb_dec]
    )


#
#    Main
#
if __name__ == "__main__":
    #
    # Get user-defined params
    #
    parser = argparse.ArgumentParser(
        prog='excel_art.py',
        description="A Python 3.10+ script which replicates a given image as a spreadsheet, by colouring each cell as the corresponding pixel.",
        usage=print_user_guide(),
    )
    parser.add_argument('--path_to_img', required=True, default=None, type=Path)
    parser.add_argument('--desired_cell_height_in_pixels', required=False, type=float, default=5.5)
    parser.add_argument('--desired_art_width_in_columns', required=False, type=int, default=100, help='must be comprised in [1; 1024]')
    parser.add_argument('--path_to_output_file', required=False, default='./excel_art.xlsx', type=Path)
    args = parser.parse_args()
    #
    # Set user-defined values
    #
    path_to_img = args.path_to_img
    desired_cell_height_in_pixels = args.desired_cell_height_in_pixels
    desired_art_width_in_columns = args.desired_art_width_in_columns
    path_to_output_file = args.path_to_output_file
    output_dir = os.path.dirname(path_to_output_file)
    output_file_name = get_file_name_from_path(path_to_output_file)
    #
    # Load image
    #
    img_width, img_height, img_pixels = (0, 0, [])
    spreadsheet_max_columns = 1024
    with Image.open(path_to_img) as img:
        img_width, img_height = img.size
        if desired_art_width_in_columns > spreadsheet_max_columns:
            print(f"Warning: 'desired_art_width_in_columns' specified was above {spreadsheet_max_columns} (max columns in a spresdsheet)")
            print(f"Warning: 'desired_art_width_in_columns' was forced to {spreadsheet_max_columns}")
            desired_art_width_in_columns = spreadsheet_max_columns
        if desired_art_width_in_columns != img_width:
            #
            # Resize image
            #
            scale_factor = desired_art_width_in_columns / img_width
            desired_art_height_in_rows = int(img_height * scale_factor)
            img = img.resize(size=(desired_art_width_in_columns, desired_art_height_in_rows))
            img_width, img_height = img.size
        if img.mode in ('RGB', 'RGBA', 'LA') or (img.mode == 'P' and 'transparency' in img.info):
            img_pixels = img.convert('RGBA').load()
    img_file_name = get_file_name_from_path(path_to_img)
    *ofname, ofext = output_file_name.split('.')
    ofname = '.'.join(ofname)
    output_file_name = f'{ofname}_{img_file_name}_rendered_as_{img_width}x{img_height}.{ofext}'
    #
    # Convert to Excel file
    #
    wb = openpyxl.Workbook()
    ws = wb.active
    with tqdm(total=img_width, desc='Image columns', position=0) as progress_bar_cols:
        with tqdm(total=img_height, desc='Image rows', position=1, leave=False) as progress_bar_rows:
            for x in range(img_width):
                ws.column_dimensions[get_column_letter(x + 1)].width = 1
                for y in range(img_height):
                    ws.row_dimensions[y + 1].height = desired_cell_height_in_pixels
                    rgb, alpha = img_pixels[x, y][:3], img_pixels[x, y][3]
                    # Cells mapped to transparent pixels should not be painted
                    if alpha == 0:
                        continue
                    rgb_hex = rgb_dec_to_rgb_hex(rgb)
                    cell_coord = get_column_letter(x + 1) + str(y + 1)
                    ws[cell_coord].fill = PatternFill(
                        start_color=rgb_hex,
                        end_color=rgb_hex,
                        fill_type='solid'
                    )
                    progress_bar_rows.update(1)
                progress_bar_cols.update(1)
    path_to_output_file = os.path.join(output_dir, output_file_name)
    print(f"Saving as {path_to_output_file!r}... ", end='')
    wb.save(path_to_output_file)
    print('OK')